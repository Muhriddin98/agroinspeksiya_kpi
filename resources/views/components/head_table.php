<style>
    table th{
        height: auto;
        white-space: normal!important;
        border: 1px solid grey!important;
    }
    table td {
        height: auto;
        white-space: normal!important;
        border: 1px solid grey!important;
    }
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->
    <div class="row page-title-header">
        <div class="col-12">
            <div class="page-header">
                <h4 class="page-title">
                        <span class="text-primary">
                            Давлат органи таркибий тузилмаси, ҳудудий ва идоравий мансуб ташкилотларнинг давлат фуқаролик хизматчиси фаолиятини ЭМСК асосида
                        </span>
                </h4>
            </div>
        </div>
    </div>
